const translations = {
	"en": {  
		'Главная':'Home',
		'Галерея': "Gallery",
		'О нас': "About us",
		'Контакты': "Contacts",
		'М Ө Ң Г Ү': 'M O N G Y',
		'Подать заявку': "Submit an application",
		'Организаторы': 'Оrganizers',
		"Общее положение":'General provisisions',
		"Цель форума": 'Goals of the Forum',
		"До начала фестиваля осталось" : 'Days remaining until the start of the festival',
		"Условия участия": 'The terms of participation',
		'Первый международный кинофорум этно-экологических фильмов...': 'The first international film forum of ethno-ecological films',
		'Явление МӨНГҮ (рус. – «ледник») – это величайщее хранилище...': 'The phenomenon of MONGU (Rus. - "glacier") ...',
		'Участниками конкурса могут быть государственные и частные студии...': 'Participants of the competition can be public and private studios...',	
		'Первый международный кинофорум «МӨНГҮ» пройдет с 3 по 7 сентября...' : 'The 1st International Film Forum "MONGU" will be held from 3rd to 7th September',
		'Присоединяйся и ты': 'Join us',
		'Продюсер кинофорума Монгу, Коммерческий директор рекламного агенства Skyline': 'The producer of the Mango Film Forum is the commercial director of the advertising agency Skyline',
		'Арт-директор кинофорума "Монгу", член Союза кинематографистов КР':'Art Director of the film forum "Mongu" is the member of the Union of filmmakers of the Republic',
		'Координатор кинофорума Монгу, ректор телеканала ТВ1, менеджер в сфере масс-медиа': 'Coordinator of the film forum "Mongu" is editor of TV channel "TV 1" and the manager in the field of mass media',
		'Директор кинофорума Монгу, член Союза кинематографистов КР, продюсер': 'Director of the film forum "Mongu" is the producer and the member of the Union of filmmakers of the Kyrgyz Republic',
		"Технический директор, член Союза кинематографистов КР, оператор - поставщик": 'Technical Director is the member of the Union of filmmakers of the Kyrgyz Republic and the director of photography',
		'Event - менеджер кинофорума "Монгу", менеджер по логистике, эксперт-консультант международных проектов':'Event-manager of the film forum "Mongu" is the logistics manager and the expert consultant of the international projects', 
		'Художественный руководитель кинофорума Монгу, кинорежиссер, один из основателей Национальной кинопремии Ак-Илбирс, председатель Союза кинематографистов КР': 'Art Director of the film forum “Mongu” is a director, one of the founders of the national film award "AK-Ilbirs", and the Chairman of the Union of filmmakers of the Kyrgyz filmmakers',
		'Первый международный кинофорум этно-экологических фильмов':'The first international film forum of ethno-ecological films',
		'Кинофорум «М Ө Ң Г Ү» это событие, вдохновляющее и объединяющее людей кому небезразлично защите окружающей среды, биокультурного разнообразия, возрождению культуры кочевых народов.' : 'The "Mongu" film forum is an event that inspires and unites people who care about the protection of the environment, biocultural diversity and the revival of the culture of nomadic people',
		'Продюсер кинофорума "Монгу", коммерческий директор рекламного агентства "Sky Line"' : 'The producer of the Mango Film Forum is the commercial director of the advertising agency Skyline',
		'Ника Темирова' : 'Nika Temirova',
		'Сторожук Айгуль' : 'Storojyk Aigul',
		'Кенеш Арген' : 'Kenesh Argen',
		'Бегайым Мамаева' : 'Begaiym Mamaeva',
		'Кулмендеев Таалайбек' : 'Kylmendeev Taalaibek',
		'Эдиль Кылычбеков' : 'Edil Kylychbekov',
		'Толобеков Талантбек' : 'Tolobekov Talantbek',
		'Ф.И.О' : 'Full name',
		'Хронометраж' : 'Timing',
		'Название фильма' : 'Title of the film',
		'Страна' : 'Country',
		'Год производства' : 'Year of production',
		'В каком формате будет представлен фильм?' : 'In what format the film will be presented?',
		'Режиссер (полное имя и фамилия)' : 'Director',
		'Автор сценария' : 'Scriptwriter',
		'Оператор': 'Camera',
		'В ролях': 'Main characters',
		'Производство (Студия)': 'Production (Studio)',
		'Кто представляет фильм на форуме?': 'Who presents the film at the forum?',
		'Синопсис': 'Synopsis',
		'Биография режиссера': "Director's biography",
		'Продюсер' : 'Producer',
		'Субтитры (английские, русские, другое)' : 'Subtitles (Eng, Russ, ets.)',
		'Язык фильма' : 'Language',
		'Фильмография режиссера' : 'Filmography of the Director',
		'123' :"Director's photo and frames from the film in JPEG format (300 pixels 10х12)",
		'Имеется ли трейлер фильма, и согласны ли Вы на его размещение на сайте форума?' : 'Do you have a movie trailer and do you agree to its placement on the forum’s website?',
		'Фото режиссера и кадры из фильма в формате JPEG (300 пикселей 10х12)' : "Director's photo and frames from the film in JPEG format (300 pixels 10х12)",
		'Далее' : 'More',
		'Отправить' : 'Submit',
		'<li>- Заполненная заявка на участие на русском и английском языках (образец прилагается);</li><li>- Фильм, в виде электронной ссылки (скачиваемый, желательно на VIMEO); </li><li>- Краткое содержание / синопсис / аннотация фильма на русском и английском языках;</li><li>- Биографию и фильмографию режиссера на русском и английском языках;</li><li>- Фотоматериалы для каталога (фото авторов и кадры из фильма в электронном виде);</li><li>- Афиша / плакат фильма;</li><li>- Трейлер для использования в рекламных, имиджевых, презентационных целях на русском и английском языках;</li><li>- Заявки принимаются до 01 - августа 2017 года</li><li>- Фильмы, присланные после указанного срока, не рассматриваются.</li>' : '<li>- Completed application form in Russian and English (sample attached); </li><li>- Film link (downloadable, desirable on VIMEO);  </li><li>- Summary / synopsis / abstract of the film in Russian and English; </li><li>- Biography and filmography of the Director in Russian and English; </li><li>- Photo materials for the catalog (photos of the authors and footage from the film in electronic form); </li><li>- Poster / poster of the film; </li><li>- Trailer for use in advertising, image, presentation purposes in Russian and English; </li><li>- Applications are accepted until 1 August 2018, by E - mail: eeiff.mongu@gmail.com </li><li>- Films sent after the deadline will not be considered. </li>',
		'Необходимые документы' : 'REQUIRED DOCUMENTS',
		'Подать заявку на участие' : 'Аpply to participate',
		'«МӨНГҮ» <br> I МЕЖДУНАРОДНЫЙ КИНОФОРУМ ЭТНО-ЭКОЛОГИЧЕСКИХ  <br> ФИЛЬМОВ <br> 3-7 СЕНТЯБРЯ 2018 ГОДА' : 'The first international film forum of ethno-ecological films - from 3 till 7 September 2018',
		'Партнеры' : 'Partners',
		'Медиа поддержка' : 'Media support',
		'Генеральный спонсор форума' : 'General sponsor of the forum',
		'Место проведения фестиваля' : 'Place of the festival',
		'Адрес' : 'Address',
		'Тел.' : 'Tel.',
		'<p>I Международный кинофорум «МӨНГҮ» пройдет с 3 по 7 сентября, в рамках Всемирных игр кочевников Кыргызской Республики. Основная программа форума пройдет в урочище Кырчын, Иссык-Кульской области, где будут организованы специальные киноюрты и площадка под открытым небом для просмотров фильмов. </p><p>А так же будут проводится спец-программы, семинары, мастер-классы с участием международных кинематографистов, экологов, экспертов в области защиты окружающей экосистемы; культурно-развлекательная программа фестиваля; и конечно же программа Всемирных Игр Кочевников.</p>' : '<p>The 1st International Film Forum "MONGU" will be held from 3rd to 7th September, within the framework of the World Nomad Games of the Kyrgyz Republic. The main program of the forum will be held in the tract Kyrchyn, in Issyk-Kul region, where special cinema-yurts and an outdoor playground  will be organized for watching films.</p><p>The program of the event includes viewing of thematic films, discussions, seminars, master classes with the participation of international filmmakers and environmentalists and experts in the field of environmental protection; cultural and entertainment program of the forum, and of course the program of the World Nomad Games.</p> ',
		'<p>Явление МӨНГҮ (рус. – «ледник») – это величайщее хранилище и источник энергии жизни всей нашей планеты. Наша планета — это огромная батарейка, у которой каждый отдельный природный ресурс имеют свой лимит. Миссия международного кинофорума «МӨНГҮ» посвящена поддержке движения по сохранению этих природных ресурсов; защите окружающей среды и биокультурного разнообразия; показать положительные и отрицательные стороны влияния «глобализации» на нашу планету и на качество жизни человечества через кинематографическое искусство; показать значение, масштаб этих вопросов и как кинематограф может помочь в их решении; презентовать зрителям лучшие кинопроизведения и создать интересную, творческую атмосферу кинематографистам и всем гостям для новых вдохновений, поисков идей; а так же дать стимул новому возрождению культуры кочевых народов, которые испокон веков жили в гармонии с природой; </p>' : '<p>The phenomenon of MONGU (Rus. - "glacier") - is a majestic storage and energy source of life for our entire planet. Our planet is a huge battery, each individual natural resource has its own limit. The mission of the international film forum "MONGU" is devoted to supporting the movement to preserve these natural resources; protecting the environment and biocultural diversity; to show the positive and negative aspects of the impact of "globalization" on our planet and the quality of life of mankind through the cinematic art; to show the importance and scale of these issues and how cinema can help in their solution; to present the best films to the audience and create an interesting, creative atmosphere for filmmakers and all guests for new inspirations and searching for ideas; as well as to give impetus to a new revival of the culture of nomadic peoples who have lived in harmony with nature.</p>',
		'<p>Участниками конкурса могут быть государственные и частные студии, кинокомпании, независимые кинематографисты, продюсеры, творческие группы и правообладатели авторских прав на предоставленные фильмы.</p>' : '<p>Participants of the competition can be public and private studios, film companies, independent filmmakers, producers, creative groups and copyright holders of the provided films.</p>',
		'<p>- В программу форума принимаются документальные, научно-популярные и учебные фильмы, созданные 2015-2018 гг. и отвечающие основной цели, тематике форума.</p><p>- Хронометраж фильмов - от 40 минут и более.</p><p>- Фильмы снятые на русском языке должны иметь субтитры на </p><p>- Фильмы снятые на русском языке должны иметь субтитры на английском языке. </p><p>- Фильмы снятые на английском языке должны иметь русские субтитры.</p><p>- Фильмы снятые на других языках должны иметь английские и русские субтитры.</p>' : '<p>The program of the forum accepts documentary, scientific-popular and educational films created in 2015-2018 and complying with the main purpose and themes of the forum.</p><p>Duration of the film must be - from 20 minutes or more.</p><p>Films made in Russian language must have English subtitles. </p><p>Films made in English language must have Russian subtitles.</p><p>Films made in other languages must have English and Russian subtitles.</p><p>Responsibility for claims and claims of third parties related to copyright and related rights lies with the legal or physical person who submitted the film to the forum in respect of which the dispute arose. </p>',
		'<p>- Отбор и формирование программы заканчивается не позднее, чем за месяц до начала мероприятия. Результаты отбора публикуются на официальном сайте форума, а также доводятся персональным уведомлением по электронной почте, указанной в заявочной форме представленного фильма.</p><p>- Организаторы форума оставляют за собой право использовать фотографии, слайды и фрагменты фильмов (продолжительностью не более 3-х минут) для публикаций в СМИ. </p><p>- Условия пребывания официальных гостей форума оговариваются в персональных приглашениях.</p><p>- Любые организации и лица, желающие участвовать в мероприятии в качестве гостей, должны известить Дирекцию о своем намерении заранее: не позднее 1 августа 2018 г. и согласовать условия участия.</p>' : '<p>The selection and formation of the program ends no later than one month before the start of the event. The results of the selection will be published on the official website of the forum, and will also be notified by e-mail, specified in the application form of the submitted film.</p><p>Forum organizers reserve the right to use photos, slides and film fragments (no longer than 3 minutes) for publication in the media.</p><p>Conditions of stay of the official guests of the forum will be specified in personal invitations.Any organizations or persons wishing to participate in the event as guests must notify the Directorate of their intention in advance: no later than 1 August, 2018 and agree on the terms of participation.</p>'
	},
	"ru": {
		'<p>- Отбор и формирование программы заканчивается не позднее, чем за месяц до начала мероприятия. Результаты отбора публикуются на официальном сайте форума, а также доводятся персональным уведомлением по электронной почте, указанной в заявочной форме представленного фильма.</p><p>- Организаторы форума оставляют за собой право использовать фотографии, слайды и фрагменты фильмов (продолжительностью не более 3-х минут) для публикаций в СМИ. </p><p>- Условия пребывания официальных гостей форума оговариваются в персональных приглашениях.</p><p>- Любые организации и лица, желающие участвовать в мероприятии в качестве гостей, должны известить Дирекцию о своем намерении заранее: не позднее 1 августа 2018 г. и согласовать условия участия.</p>': '<p>- Отбор и формирование программы заканчивается не позднее, чем за месяц до начала мероприятия. Результаты отбора публикуются на официальном сайте форума, а также доводятся персональным уведомлением по электронной почте, указанной в заявочной форме представленного фильма.</p><p>- Организаторы форума оставляют за собой право использовать фотографии, слайды и фрагменты фильмов (продолжительностью не более 3-х минут) для публикаций в СМИ. </p><p>- Условия пребывания официальных гостей форума оговариваются в персональных приглашениях.</p><p>- Любые организации и лица, желающие участвовать в мероприятии в качестве гостей, должны известить Дирекцию о своем намерении заранее: не позднее 1 августа 2018 г. и согласовать условия участия.</p>',
		'<p>Явление МӨНГҮ (рус. – «ледник») – это величайщее хранилище и источник энергии жизни всей нашей планеты. Наша планета — это огромная батарейка, у которой каждый отдельный природный ресурс имеют свой лимит. Миссия международного кинофорума «МӨНГҮ» посвящена поддержке движения по сохранению этих природных ресурсов; защите окружающей среды и биокультурного разнообразия; показать положительные и отрицательные стороны влияния «глобализации» на нашу планету и на качество жизни человечества через кинематографическое искусство; показать значение, масштаб этих вопросов и как кинематограф может помочь в их решении; презентовать зрителям лучшие кинопроизведения и создать интересную, творческую атмосферу кинематографистам и всем гостям для новых вдохновений, поисков идей; а так же дать стимул новому возрождению культуры кочевых народов, которые испокон веков жили в гармонии с природой; </p>' : '<p>Явление МӨНГҮ (рус. – «ледник») – это величайщее хранилище и источник энергии жизни всей нашей планеты. Наша планета — это огромная батарейка, у которой каждый отдельный природный ресурс имеют свой лимит. Миссия международного кинофорума «МӨНГҮ» посвящена поддержке движения по сохранению этих природных ресурсов; защите окружающей среды и биокультурного разнообразия; показать положительные и отрицательные стороны влияния «глобализации» на нашу планету и на качество жизни человечества через кинематографическое искусство; показать значение, масштаб этих вопросов и как кинематограф может помочь в их решении; презентовать зрителям лучшие кинопроизведения и создать интересную, творческую атмосферу кинематографистам и всем гостям для новых вдохновений, поисков идей; а так же дать стимул новому возрождению культуры кочевых народов, которые испокон веков жили в гармонии с природой; </p>' ,
		'<p>- В программу форума принимаются документальные, научно-популярные и учебные фильмы, созданные 2015-2018 гг. и отвечающие основной цели, тематике форума.</p><p>- Хронометраж фильмов - от 40 минут и более.</p><p>- Фильмы снятые на русском языке должны иметь субтитры на </p><p>- Фильмы снятые на русском языке должны иметь субтитры на английском языке. </p><p>- Фильмы снятые на английском языке должны иметь русские субтитры.</p><p>- Фильмы снятые на других языках должны иметь английские и русские субтитры.</p>': '<p>- В программу форума принимаются документальные, научно-популярные и учебные фильмы, созданные 2015-2018 гг. и отвечающие основной цели, тематике форума.</p><p>- Хронометраж фильмов - от 40 минут и более.</p><p>- Фильмы снятые на русском языке должны иметь субтитры на </p><p>- Фильмы снятые на русском языке должны иметь субтитры на английском языке. </p><p>- Фильмы снятые на английском языке должны иметь русские субтитры.</p><p>- Фильмы снятые на других языках должны иметь английские и русские субтитры.</p>',
		'<p>Участниками конкурса могут быть государственные и частные студии, кинокомпании, независимые кинематографисты, продюсеры, творческие группы и правообладатели авторских прав на предоставленные фильмы.</p>' : '<p>Участниками конкурса могут быть государственные и частные студии, кинокомпании, независимые кинематографисты, продюсеры, творческие группы и правообладатели авторских прав на предоставленные фильмы.</p>',
		'Главная':'Главная',
		'Галерея': 'Галерея',
		'О нас': 'О нас',
		'Контакты': 'Контакты',
		'М Ө Ң Г Ү': 'М Ө Ң Г Ү',
		'Подать заявку': 'Подать заявку',
		'Организаторы': 'Организаторы',
		"Общее положение": "Общее положение",
		"До начала фестиваля осталось" : "До начала фестиваля осталось",
		'Первый международный  кинофорум этно-экологических  фильмов': 'Первый международный  кинофорум этно-экологических  фильмов',			
		"Цель форума":"Цели форума",
		'Условия участия': 'Условия участия',
		'Участниками конкурса могут быть государственные и частные студии...' : 'Участниками конкурса могут быть государственные и частные студии...',
		'Явление МӨНГҮ (рус. – «ледник») – это величайщее хранилище...' : 'Явление МӨНГҮ (рус. – «ледник») – это величайщее хранилище...',
		'Первый международный кинофорум «МӨНГҮ» пройдет с 3 по 7 сентября...': 'Первый международный кинофорум «МӨНГҮ» пройдет с 3 по 7 сентября...',
		'Первый международный кинофорум этно-экологических фильмов' : 'Первый международный кинофорум этно-экологических фильмов',
		'Ника Темирова' : 'Ника Темирова',
		'Арт-директор кинофорума "Монгу", член Союза кинематографистов КР' : 'Арт-директор кинофорума "Монгу", член Союза кинематографистов КР',
		'Сторожук Айгуль' : 'Сторожук Айгуль',
		'Координатор кинофорума Монгу, ректор телеканала ТВ1, менеджер в сфере масс-медиа': 'Координатор кинофорума Монгу, ректор телеканала ТВ1, менеджер в сфере масс-медиа',
		'Эдиль Кылычбеков' : 'Эдиль Кылычбеков',
		'Продюсер кинофорума "Монгу", коммерческий директор рекламного агентства "Sky Line"': 'Продюсер кинофорума "Монгу", коммерческий директор рекламного агентства "Sky Line"',
		'Толобеков Талантбек' : 'Толобеков Талантбек',
		'Директор кинофорума Монгу, член Союза кинематографистов КР, продюсер' : 'Директор кинофорума Монгу, член Союза кинематографистов КР, продюсер',
		'Кенеш Арген' : 'Кенеш Арген',
		'Технический директор, член Союза кинематографистов КР, оператор - поставщик' : 'Технический директор, член Союза кинематографистов КР, оператор - поставщик',
		'Бегайым Мамаева' : 'Бегайым Мамаева',
		'Event - менеджер кинофорума "Монгу", менеджер по логистике, эксперт-консультант международных проектов' : 'Event - менеджер кинофорума "Монгу", менеджер по логистике, эксперт-консультант международных проектов',
		'Кулмендеев Таалайбек' : 'Кулмендеев Таалайбек',
		'Художественный руководитель кинофорума Монгу, кинорежиссер, один из основателей Национальной кинопремии Ак-Илбирс, председатель Союза кинематографистов КР' : 'Художественный руководитель кинофорума Монгу, кинорежиссер, один из основателей Национальной кинопремии Ак-Илбирс, председатель Союза кинематографистов КР',
		'Кинофорум «М Ө Ң Г Ү» это событие, вдохновляющее и объединяющее людей кому небезразлично защите окружающей среды, биокультурного разнообразия, возрождению культуры кочевых народов.' : 'Кинофорум «М Ө Ң Г Ү» это событие, вдохновляющее и объединяющее людей кому небезразлично защите окружающей среды, биокультурного разнообразия, возрождению культуры кочевых народов.',
		'Галерея' : 'Галерея',
		'Присоединяйся и ты' : 'Присоединяйся и ты',
		'Генеральный спонсор форума' : 'Генеральный спонсор форума',
		'Медиа поддержка' : 'Медиа поддержка',
		'Партнеры' : 'Партнеры',
		'Место проведения фестиваля' : 'Место проведения фестиваля',
		'Ф.И.О' : 'Ф.И.О',
		'Название фильма' : 'Название фильма',
		'Страна' : 'Страна',
		'Хронометраж' : 'Хронометраж',
		'Год производства' : 'Год производства',
		'В каком формате будет представлен фильм?' : 'В каком формате будет представлен фильм?',
		'Режиссер (полное имя и фамилия)' : 'Режиссер (полное имя и фамилия)',
		'Автор сценария' : 'Автор сценария',
		'Оператор' : 'Оператор',
		'В ролях': 'В ролях',
		'Производство (Студия)': 'Производство (Студия)',
		'Кто представляет фильм на форуме?': 'Кто представляет фильм на форуме?',
		'Синопсис': 'Синопсис',
		'Биография режиссера': 'Биография режиссера',
		'Продюсер' : 'Продюсер',
		'Субтитры (английские, русские, другое)' : 'Субтитры (английские, русские, другое)',
		'Язык фильма' : 'Язык фильма',
		'Фильмография режиссера' : 'Фильмография режиссера',
		'Имеется ли трейлер фильма, и согласны ли Вы на его размещение на сайте форума?': 'Имеется ли трейлер фильма, и согласны ли Вы на его размещение на сайте форума?',
		'Фото режиссера и кадры из фильма в формате JPEG (300 пикселей 10х12)' : 'Фото режиссера и кадры из фильма в формате JPEG (300 пикселей 10х12)',
		'Отправить' : 'Отправить',
		'Далее' : 'Далее',
		"<li>- Заполненная заявка на участие на русском и английском языках (образец прилагается);</li><li>- Фильм, в виде электронной ссылки (скачиваемый, желательно на VIMEO); </li><li>- Краткое содержание / синопсис / аннотация фильма на русском и английском языках;</li><li>- Биографию и фильмографию режиссера на русском и английском языках;</li><li>- Фотоматериалы для каталога (фото авторов и кадры из фильма в электронном виде);</li><li>- Афиша / плакат фильма;</li><li>- Трейлер для использования в рекламных, имиджевых, презентационных целях на русском и английском языках;</li><li>- Заявки принимаются до 01 - августа 2017 года</li><li>- Фильмы, присланные после указанного срока, не рассматриваются.</li>" : '<li>- Заполненная заявка на участие на русском и английском языках (образец прилагается);</li><li>- Фильм, в виде электронной ссылки (скачиваемый, желательно на VIMEO); </li><li>- Краткое содержание / синопсис / аннотация фильма на русском и английском языках;</li><li>- Биографию и фильмографию режиссера на русском и английском языках;</li><li>- Фотоматериалы для каталога (фото авторов и кадры из фильма в электронном виде);</li><li>- Афиша / плакат фильма;</li><li>- Трейлер для использования в рекламных, имиджевых, презентационных целях на русском и английском языках;</li><li>- Заявки принимаются до 01 - августа 2017 года</li><li>- Фильмы, присланные после указанного срока, не рассматриваются.</li>',
		'Необходимые документы': 'Необходимые документы',
		'Подать заявку на участие' : 'Подать заявку на участие',
		"«МӨНГҮ» <br> I МЕЖДУНАРОДНЫЙ КИНОФОРУМ ЭТНО-ЭКОЛОГИЧЕСКИХ  <br> ФИЛЬМОВ <br> 3-7 СЕНТЯБРЯ 2018 ГОДА": "«МӨНГҮ» <br> I МЕЖДУНАРОДНЫЙ КИНОФОРУМ ЭТНО-ЭКОЛОГИЧЕСКИХ  <br> ФИЛЬМОВ <br> 3-7 СЕНТЯБРЯ 2018 ГОДА",
		'Генеральный спонсор форума' : 'Генеральный спонсор форума',
		'Медиа поддержка' :'Медиа поддержка',
		'Партнеры' : 'Партнеры',
		'Место проведения фестиваля':'Место проведения фестиваля',
		'Адрес' : 'Адрес',
		'Тел.' : 'Тел.',
		'<p>I Международный кинофорум «МӨНГҮ» пройдет с 3 по 7 сентября, в рамках Всемирных игр кочевников Кыргызской Республики. Основная программа форума пройдет в урочище Кырчын, Иссык-Кульской области, где будут организованы специальные киноюрты и площадка под открытым небом для просмотров фильмов. </p><p>А так же будут проводится спец-программы, семинары, мастер-классы с участием международных кинематографистов, экологов, экспертов в области защиты окружающей экосистемы; культурно-развлекательная программа фестиваля; и конечно же программа Всемирных Игр Кочевников.</p>' : '<p>I Международный кинофорум «МӨНГҮ» пройдет с 3 по 7 сентября, в рамках Всемирных игр кочевников Кыргызской Республики. Основная программа форума пройдет в урочище Кырчын, Иссык-Кульской области, где будут организованы специальные киноюрты и площадка под открытым небом для просмотров фильмов. </p><p>А так же будут проводится спец-программы, семинары, мастер-классы с участием международных кинематографистов, экологов, экспертов в области защиты окружающей экосистемы; культурно-развлекательная программа фестиваля; и конечно же программа Всемирных Игр Кочевников.</p>'
		

	}
};
const translate = function (language) {
	language = translations[language];
	if (!language) {
		return;
	}
	$('.translate').each(function () {
		let original = $(this).data('original');
		if (!original) {
			original = $(this).html();
		}
		$(this).html(language[original]);
		$(this).data('original', original);
	});
	$('.translatei').each(function () {
		let original = $(this).data('original');
		if (!original) {
			original = $(this).attr('placeholder', original);
		}
		$(this).attr('placeholder', language[original]);
		$(this).data('original', original);
	});

	$('.translatet').each(function () {
		$(this).attr('placeholder', '');
		let original = $(this).data('original');
		if (!original) {
			original = $(this).attr('placeholder', original);
		}
		$(this).attr('placeholder', language[original]);
		$(this).data('original', original);
	});

};

jQuery(document).ready(function($) {
	$('.slider-main').owlCarousel({
	    margin: 10,
	    nav: false,
	    dots: true,
	    items: 1,
	    animateIn: 'fadeIn',
	    animateOut: 'fadeOut',
	    autoplay: true,
	    autplayTimeout: 8000,
	    mouseDrag: false,
	    touchDrag: true,
	    loop: true,
	    responsive: {
			0: {
				smartSpeed: 500
			},
			992: {
				smartSpeed: 4000,
			}
		}
	})

	

	$('.owl-org').owlCarousel({
		loop: true,
		nav: false,
		dots: false,
		center: true,
		smartSpeed: 2000,
		items: 3,
		autoplay: true,
		autoplayTimeout: 4000,
		responsive: {
			0: {
				items: 1,
				center: false
			},
			992: {
				items: 3
			}
		}
	})

	$('#custom-controls > div').on('click', function () {
	    $('.owl-org').trigger('stop.owl.autoplay');
	});

	$('#custom-controls2 > div').on('click', function () {
	    $('.owl-prod').trigger('stop.owl.autoplay');
	});

	$('.owl-dot').on('click', function () {
	    $('.slider-main').trigger('stop.owl.autoplay');
	});
	
	$('.owl-prod').owlCarousel({
		loop: true,
	    nav: false,
	    dots: false,
	    center: true,
	    items: 3,
	    animateIn: 'fadeIn',
	    animateOut: 'fadeOut',
	    smartSpeed: 2500,
	    autoplayTimeout: 7500,
	    responsive: {
			0: {
				items: 1,
				center: false
			},
			992: {
				items: 3
			}
		}
	})

	$('.owl-popup').owlCarousel({
		loop: true,
	    nav: false,
	    dots: true,
	    items: 1,
	    dotsContainer: '.owl-dots-new',
	    responsive: {
			0: {
				autoHeight: true,
				center: false
			},
			992: {
				autoHeight: false
			}
		}

	})
	$('.owl-dots-new .owl-dot').click(function () {
	  $('.owl-popup').trigger('to.owl.carousel', [$(this).index(), 300]);
	});

	var owl = $('.owl-org');
	// Go to the next item
	$('#custom-controls .next').click(function() {
	    owl.trigger('next.owl.carousel');
	})
	// Go to the previous item
	$('#custom-controls .prev').click(function() {
	    // With optional speed parameter
	    // Parameters has to be in square bracket '[]'
	    owl.trigger('prev.owl.carousel', [300]);
	})

	var owl2 = $('.owl-prod');
	// Go to the next item
	$('#custom-controls2 .next').click(function() {
	    owl2.trigger('next.owl.carousel');
	})
	// Go to the previous item
	$('#custom-controls2 .prev').click(function() {
	    // With optional speed parameter
	    // Parameters has to be in square bracket '[]'
	    owl2.trigger('prev.owl.carousel', [300]);
	})

	var owl3 = $('.owl-popup');
	// Go to the next item
	$('.owl-custom-nav .owl-next').click(function() {
	    owl3.trigger('next.owl.carousel');
	})
	// Go to the previous item
	$('.owl-custom-nav .owl-prev').click(function() {
	    // With optional speed parameter
	    // Parameters has to be in square bracket '[]'
	    owl3.trigger('prev.owl.carousel', [300]);
	})

	$(window).scroll(function(event) {
		var st = $(window).scrollTop(),
			ah = $('#block1').height() + $('#block2').height() + $('#block3').height();

		if(st >= ah - $(this).height() - 220) {
			$('#block4 img').css({
				bottom: '-' + st/10 + '%'
			});
		} else {
			$('#block4 img').css({
				transition: '.4s ease',
				bottom: 0
			});
		}
		
	});

	$('.more__1').click(function(event) {
		$(this).hide()
		$('.content-none').slideDown('slow');
		$('.exit__big').show();
		$('.more__active').show();
	});

	$('.exit__big').click(function(event) {
		$(this).hide();
		$('.more__active').hide();
		$('.more__1').show();
		$('.content-none').slideUp('slow');
		$('form input').val('');
		$('form textarea').val('');
		$('#file__list').text('');
	});

	var countDownDate = new Date("Sep 5, 2018 15:37:25").getTime();

// Update the count down every 1 second
	var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  if(seconds < 10) {
  	seconds = "0" + seconds;
  }

  // Display the result in the element with id="demo"
  document.getElementById("clock").innerHTML = days + " : " + hours + " : "
  + minutes + " : " + seconds;

  // If the count down is finished, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("clock").innerHTML = "EXPIRED";
  }
}, 1000);

	$(function(){
		$(window).scroll(function() {
			var top = $(document).scrollTop();
			if (top < $('.l-menu').height() ) $(".l-menu").removeClass('fixed').fadeIn();
			else $(".l-menu").addClass('fixed').fadeIn();
		});
	});

	 // setInterval (blink, 1000 );

	 // function blink() {
	 //  var inout = 500;
	 //  $("#clock").fadeIn(inout).fadeOut(inout);
	 // }


	 $('body').on("click","a.go_to", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = ($(id).offset().top) - 80;
        // alert(top+130);
        $('body,html').animate({scrollTop: top}, 1500);
    });

	 $('[data-fancybox="gallery"]').fancybox({
		thumbs: {
			autoStart: true,
			axis: 'x'
		},
		buttons: ['close'],

		btnTpl: {
			arrowLeft: '<a data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" href="javascript:;"><img src="img/left-arrow.png" alt="" /></a>',
			arrowRight: '<a data-fancybox-next class="fancybox-button fancybox-button--arrow_right" href="javascript:;"><img src="img/right-arrow.png" alt="" /></a>'
		}
	});

	 $('.menu-mob').click(function(event) {
	 	$('.menu').slideToggle('slow');
	 });

	var placeholder = 'Имеется ли трейлер фильма, и согласны ли Вы на его \n размещение на сайте форума?';
	$('textarea').attr('placeholder', placeholder);

	$('#filesToUpload').change(function(event) {
		var input = document.getElementById('filesToUpload');
		var list = document.getElementById('file__list');

		//empty list for now...
		while (list.hasChildNodes()) {
			list.removeChild(list.firstChild);
		}

		var li = document.createElement('li');
		var	cls = document.createElement('div');
		cls.setAttribute('class', 'cls');
		cls.innerHTML = '<img src="img/exit.png" alt="" />';
		li.innerHTML = 'Files Load: ' + input.files.length;
		li.append(cls);
		list.append(li);

		$('.cls').click(function(event) {
			$('#filesToUpload').val('');
			$('#file__list').children('li').remove();
		});

		
	});
	
	

	$(window).scroll(function(event) {
		var st2 = $(window).scrollTop();
		if (st2 > $('#block1').height()) {
			$('#top').fadeIn(1000);
		} else {
			$('#top').fadeOut(1000);
		}
		
	});

	

	if ($('.slider-main .active > item__hidden').is(':visible')) {
		$('.title__description').hide();
	}
	$(function () {

		$('#RU').click(function (e) {
			$(this).hide();
			$('#EN').show();
			$('.logo img').attr('src', 'img/logo-ru.png');
			translate($(this).attr('rel'));		
		});


		$('#EN').click(function (e) {
			translate($(this).attr('rel'));
			$('.logo img').attr('src', 'img/logo-en.png');
			$(this).hide();
			$('#RU').show();
		});


	});

	

});


$(function(){
	'use strict';
	$('#form').on('submit', function(e) {
		e.preventDefault();
		var fd = new FormData( this );
		$.ajax({
			url: 'send.php',
			type: 'POST',
			contentType: false, 
			processData: false, 
			data: fd,
			success: function(msg) {
				if(msg == 'ok') {
					$('.more__active').val('Отправлено'); 
				} else {
					$('.more__active').val('Ошибка');
					setTimeout(function() {$('.more__active').val('Отправить');}, 3000);
				}
			}
		});
	});
});